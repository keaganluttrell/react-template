const mongooseConnection = require('mongoose');

mongooseConnection.connect('mongodb://localhost:27017/emr_test', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongooseConnection.connection.on('connected', () => {
    console.log('Mongoose connected to MongoDB');
});

module.exports = mongooseConnection;