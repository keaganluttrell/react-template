require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const STATIC_FILES = path.resolve('public');
const PORT = process.env.PORT


const app = express();

app.use(morgan('tiny'));
app.use(express.static(STATIC_FILES));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(PORT, console.log('listening', PORT));

const controller = require('./controller');

app.route('/questions')
  .get(controller.getQuestions)
  .post(controller.addQuestion);